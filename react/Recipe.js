import IngredientList from './IngredientList'
import Instructions from './Instructions'

const Recipe = ({name, ingredient, steps}) =>
    <section id={name.toLowerCase().replace(/ /g, '-')}>
        <h1>{name}</h1>
        <IngredientList list={ingredients} />
        <Instructions 
            title="Cooking instructions"
            steps={steps} />
    </section>

export default Recipe
