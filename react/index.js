import React from 'react'
import {render} from 'react-dom'
import Menu from './Menu'

var data = [
    {
        "name": "Baked Salmon",
        "ingredients": [
            { "name": "www", "amount": 1, "measurement": "www"},
            { "name": "www", "amount": 1, "measurement": "www"},
            { "name": "www", "amount": 1, "measurement": "www"}
        ],
        "steps": [
            "eee",
            "ererr"
        ]
    },
    {
        "name": "Baked Salmon2",
        "ingredients": [
            { "name": "eee", "amount": 1, "measurement": "eee"},
            { "name": "eee", "amount": 1, "measurement": "eee"},
            { "name": "eee", "amount": 1, "measurement": "eee"},
            { "name": "eee", "amount": 1, "measurement": "eee"}
        ],
        "steps": [
            "eee",
            "ererr"
        ]
    },
]

window.React = React;
render(
    <Menu recipes={data} />,
    document.getElementById('react-container')
)

