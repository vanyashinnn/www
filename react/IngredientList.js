import Ingredient from './Ingredient'
const IngredientList = ({list}) =>
    <ul classname="ingredients">
        {
            list.map(
                (ingredient, i) =>
                <Ingredient key={i} {...ingredient}/>
            )
        }
    </ul>
    
export default IngredientList
