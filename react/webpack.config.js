module.export = {
    entry: "./index.js",
    output: {
        path: "./",
        filename: "bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.js/,
                exclude: /(node_modules)/,
                loader: ['babel-loader'],
                query: {
                    presets: ['env', 'stage-0', 'react']
                }
            }
        ]
    }
} 
